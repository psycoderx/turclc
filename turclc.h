// See end of file for the license information

//   turclc.c
// Tiny URCL Compiler API

#ifndef TURCLC_H
#define TURCLC_H

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

////////////////////////////////////////////////////////////////
// DEFINES
////////////////////////////////////////////////////////////////

// I, D, S1, S2
#define STMT_MAXLEN (4)

// Common segment alignment
#define SEG_ALIGN (0x1000)

//
#define BASE_ADDR (0x00400000)

////////////////////////////////////////////////////////////////
// TYPES
////////////////////////////////////////////////////////////////

// Token type
enum {
  T_INVALID,
  T_EOF,
  T_ID,
  T_LABEL,
  T_PORT,
  T_INTLIT,
  T_STRLIT,
  T_RELPTR,
  T_MEMPTR,
  T_REGPTR,
  //
  T_NEWLINE,
  T_LESS,  // <=
  T_EQUAL, // ==
  T_MORE,  // >=
  T_BRACK_BEG,
  T_BRACK_END,
  // prefix tokens
  T_DOT,     // T_LABEL
  T_PERCENT, // T_PORT
  T_AT,      // T_INTLIT or T_STRLIT
  T_TILDE,   // T_RELPTR
  T_SHARP,   // T_MEMPTR
  T_DOLLAR,  // T_REGPTR
  T_BACKSLASH, // to ignore T_NEWLINE
  // Keywords
  T_DEFINE,
  T_PC,
  T_DW,
  T_RUN,
  T_RAM,
  T_ROM,
  // vars
  T_VBEG,
  T_BITS,
  T_MINREG,
  T_MINHEAP,
  T_MINSTACK,
  T_HEAP,
  T_MSB,
  T_SMSB,
  T_MAX,
  T_SMAX,
  T_UHALF,
  T_LHALF,
  T_VEND,
  // Instructions
  T_IBEG,
  T_ADD,
  T_SUB,
  T_RSH,
  T_LOD,
  T_STR,
  T_BGE,
  T_NOR,
  T_IMM,
  T_JMP,
  T_IN,
  T_OUT,
  T_HLT,
  T_BLE,
  T_BRG,
  T_BRL,
  T_NOP,
  T_MOV,
  T_BRE,
  T_BNE,
  T_IEND,
  //
  T_COUNT
};

// Port type
enum {
  PORT_UNKNOW,
  PORT_TEXT,
  PORT_ADDR,
  PORT_BUS,
  PORT_COUNT
};

// Segments
enum {
  SEG_UNKNOWN,
  SEG_CODE,
  SEG_DWS,
  SEG_COUNT
};

// Preperation type
enum {
  // Nothing
  P_NO,
  // Use Y as A if the stmt is too short
  P_YTOA,
  // Compare with 0 by default
  P_CMPZ,
  P_COUNT
};

// Backpatch type
enum {
  // code or dw address
  BP_LABEL,
  // heap address
  BP_MEMPTR,
  // register or buffer pointer
  BP_REGPTR,
  // relative address
  BP_RELPTR,
  // the real address of data segment
  BP_DATAPTR,
  //
  BP_COUNT
};

// x86-64 register
enum {
  TREG_RAX = 0,
  TREG_RCX = 1,
  TREG_RDX = 2,
  TREG_RBX = 3,
  TREG_COUNT
};

typedef void(*Entry_Func)(void);

// Bytes that are part of the output
typedef struct Segment {
  uint8_t *bytes;
  int size;
  int capacity;
} Segment;

// Tells what token and where
typedef struct Token_Info {
  // Token type
  int type;
  // Token row
  int row;
  // Token column
  int col;
  // Token string index
  int si;
  // Token integer
  int64_t num;
} Token_Info;

// Self contained macro definition
// or just a part of something bigger
typedef struct Macro_Atom {
  // The name of the macro this atom is a part of
  int name_si;
  // The value of this atom
  Token_Info tok;
} Macro_Atom;

// Named address for the linking purposes
typedef struct Label_Info {
  // The first token where this label is used
  Token_Info usedtok;
  // The first token where this label is declared
  Token_Info decltok;
  // The name of this label.
  // It is unique for each label
  int name_si;
  // The segment where this label is located (SEG_DWS or SEG_CODE).
  // SEG_UNKNOWN if is_defined == false
  int seg_type;
  // How much bytes in the segment are before this label.
  // 0 if is_defined == false
  int offset;
  // This label is declared on some line.
  // But the next code line is unknow at the moment
  bool is_declared;
  // This label is defined depending of what comes after it
  bool is_defined;
  // This label is used at least once
  bool is_used;
} Label_Info;

typedef struct Instr {
  // Token type
  int type;
  // Required number of tokens in the stmt
  int len;
  // 3 chars to define types
  const char *args;
  // Preperation type
  int prep;
  // Code generator
  void(*func)(const struct Instr *me);
} Instr;

// The information needed to comeback later and fix some bytes
typedef struct Backpatch {
  // Used for error messages
  Token_Info ctxtok;
  // In which segment to backpatch
  int seg_type;
  // Position in the segment where to backpatch
  int offset;
  // How many bytes to modify at the position (8 or 4)
  int size;
  // Type of this backpatch
  int type;
  // The value to write
  union {
    // BP_LABEL
    int label_name_si;
    // BP_MEMPTR
    int memptr_index;
    // BP_REGPTR
    int regptr_index;
    // BP_RELPTR
    int relptr_index;
  } value;
} Backpatch;

// All global variables in one struct
typedef struct Global_Vars {
  // Look-up token
  Token_Info tok;
  // Instruction token
  Token_Info itok;
  // Dest token
  Token_Info ytok;
  // Source1 token
  Token_Info atok;
  // Source2 token
  Token_Info btok;
  // Defined words
  Segment s_dws;
  // Instructions
  Segment s_code;
  // Look-up character
  int chr;
  // Look-up character row
  int chr_row;
  // Look-up character column
  int chr_col;
  // "turclc"
  const char *compiler_name;
  // <source>
  const char *source_filename;
  // [output] or "a.out" by default
  const char *output_filename;
  // A stream to read <source>
  FILE *source_file;
  // Defined strings
  const char **strings;
  // The number of defined strings
  int strings_num;
  // The maximum number of defined strings
  int strings_capacity;
  // Defined macros
  Macro_Atom *macros;
  // The number of defined macros
  int macros_num;
  // The active number of defined macros
  int macros_num_active;
  // The maximum number of defined macros
  int macros_capacity;
  // Labels
  Label_Info *labels;
  // The number of unique labels
  int labels_num;
  // The maximum number of labels
  int labels_capacity;
  // Every instruction offset
  int *ioffsets;
  // The number of instructions
  int ioffsets_num;
  // The maximum number of instructions
  int ioffsets_capacity;
  // Backpatchs
  Backpatch *bplist;
  // The number of backpatchs
  int bplist_num;
  // The maximum number of backpatchs
  int bplist_capacity;
  // BITS
  int h_bits;
  // MINHEAP
  int h_minheap;
  // MINREG
  int h_minreg;
  // MINSTACK
  int h_minstack;
  // -run
  bool f_run;
  // -fnoescseq
  bool f_noescseq;
  // -f_start
  bool f_start;
  // -pedantic
  bool f_pedantic;
  //
  uint64_t entry_ptr;
  uint64_t head_ptr;
  uint64_t code_ptr;
  uint64_t temp_ptr;
  uint64_t data_ptr;
  uint64_t head_size;
  uint64_t code_size;
  uint64_t temp_size;
  uint64_t data_size;
} Global_Vars;

////////////////////////////////////////////////////////////////
// LEXER FUNCTIONS
////////////////////////////////////////////////////////////////

// Do this once you have <source> and before reading tokens
void init_lexer(const char *source_filename);

// Take the character after a backslash
// and return the corresponding value.
// Returns -1 on fail
int escape_sequence(int chr);

// Get the value of the hexadecimal digit character.
// Works for every base from 2 ro 16.
// Returns a number from 0 to 15 on success or -1 on fail
int char_to_digit(int digit_char);

// Parse decimal, binary, octal, or hexadecimal integer.
// Set *invalid_out if fail.
// Returns the value of the parsed integer
int64_t parse_int(const char *string, int string_length, bool *invalid_out);

// Read the next token using read_token_join.
// Expand macros.
// Returns token type, but never T_INVALID
int read_token(void);

// Read the next token using read_token_solid.
// Join pairs like T_PERCENT + T_ID to be T_PORT.
// Get compiler variables as integers.
// Save the token information into `G->tok`.
// Returns token type, but never T_INVALID
int read_token_join(void);

// Read the next raw token using read_token_maybe.
// Save the token information into `G->tok`.
// Returns token type AS IS, but never T_INVALID
int read_token_solid(void);

// Read the next raw token.
// Save the token information into `G->tok`.
// Returns token type (may return T_INVALID)
int read_token_maybe(void);

// Read the next look-up character.
// Returns the character or EOF
int read_char(void);

// Check if the token is an instruction
bool is_instruction(int tok_type);

// Check if the token is a compiler variable
bool is_cvariable(int tok_type);

// Get the value of the variable by name.
// Where the name is a token type like T_BITS
int64_t get_compiler_variable(int tok_type);

////////////////////////////////////////////////////////////////
// PARSER FUNCTIONS
////////////////////////////////////////////////////////////////

// Check if the token has a valid argument type 
void check_type(Token_Info tok, char type);

// Compute the value to write or die trying
uint64_t bp_cumpute_value(const Backpatch *bp);

// Create a new backpatch and add it to the list.
// Modify the returned bp to provide context specific information
Backpatch *add_bp(int bp_type, int seg_type, int offset, int size);

// Assemble all segments into one ELF executable file
void save_as_elf_executable(void);

// Binary operation (instruction)
void i_binary(const Instr *me);

// Intrinsic IN (instruction)
void i_input(const Instr *me);

// Conditional or unconditional jump (instruction)
void i_jump(const Instr *me);

// Intrinsic OUT (instruction)
void i_output(const Instr *me);

// Anything (instruction)
void i_special(const Instr *me);

// Unary operation (instruction)
void i_unary(const Instr *me);

// Take a pointer and load the value from RAM.
// Put the value into the target register
void gen_arg_as_load(Token_Info tok, int treg);

// Store the value of the target register to the URCL memory
void gen_treg_to_mem(Token_Info tok, int treg);

// Store the value of the target register to the URCL register
void gen_treg_to_reg(int regi, int treg);

// Take a register, constant or address and put the value into the target register
void gen_arg_as_number(Token_Info tok, int treg);

// Get the full instruction description by instruction token
const Instr *get_instruction_info(int tok_type);

// Define all segments that are waiting to be defined
void define_declared_labels(int seg_type, int offset);

// Compile one token from DW
void compile_dw_value(void);

// Add a token to the macro by name
void add_macro_atom(int name_si, Token_Info tok);

// Skip newlines and preprocessor directives.
// After calling this function the look-up token will be one of:
//  - instruction;
//  - label;
//  - header;
//  - DW;
//  - unexpected token
void next_code_line(void);

// Keep interpreting headers until the first non-header line.
// @DEFINE also interpreted even if it's non-header
void read_headers(void);

// Interpret everything after the headers
void read_body(void);

// Expect the look-up token to be of that type.
// Throw an error if it's not
void expect_token(int type, const char *name);

// Save the instruction position in the list for the future 
void add_instruction_offset(int ioffset);

////////////////////////////////////////////////////////////////
// CORE FUNCTIONS
////////////////////////////////////////////////////////////////

// Make size bigger if needed to auto-align whatever placed after `size` bytes
uint64_t align_size(uint64_t size);

// Print -help information
void print_help(void);

// Print -version information
void print_version(void);

// Get the label by name
Label_Info *get_label(int name_si);

// Get the port type by name.
int64_t get_port_type(int name_si);

// Init the segment. Allocate `capacity` bytes
void seg_init(Segment *seg, int capacity);

// Write ELF program header entry
void seg_put_phentry(Segment *seg, int type, int prot, int offset, int size);

// fputc equivalent for segments
void seg_put_byte(Segment *seg, uint8_t byte);

// Put 16-bit integer as little endian to the segment
void seg_put_le16(Segment *seg, uint64_t data);

// Put 32-bit integer as little endian to the segment
void seg_put_le32(Segment *seg, uint64_t data);

// Put 64-bit integer as little endian to the segment
void seg_put_le64(Segment *seg, uint64_t data);

// fwrite equivalent for segments
void seg_write(Segment *seg, const void *bytes, int size);

// Put zeros to the segment
void seg_put_zeros(Segment *seg, int count);

// Convert the string to the string index.
// The same sequence of characters always gives the same string index.
// Returns a valid string index
int get_si(const char *str);

// Convert the string index back to the string.
// Returns a valid string
const char *get_str(int si);

// A malloc equivalent that never returns NULL
void *solid_malloc(size_t size);

// A malloc equivalent that never returns NULL.
// Initializes all bytes to zero
void *solid_malloc_zeros(size_t size);

// A realloc equivalent that never returns NULL
void *solid_realloc(void *ptr, size_t newsize);

// Compare strings ignoring the difference between
// lower case and upper case
int strcmp_icase(const char *s1, const char *s2);

////////////////////////////////////////////////////////////////
// VARIABLES
////////////////////////////////////////////////////////////////

#endif

/*
MIT License

Copyright (c) 2024 Artem Pirunov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

