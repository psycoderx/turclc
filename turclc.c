// See end of file for the license information

//   turclc.c
// Tiny URCL Compiler

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#include "turclc.h"

////////////////////////////////////////////////////////////////
// DEFINES
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// TYPES
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// VARIABLES
////////////////////////////////////////////////////////////////

// Globals. Do NOT access it directly
static Global_Vars static_G;

// Vector to all globals
Global_Vars *G = &static_G;

////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  // Init static globals
  memset(G, 0, sizeof(*G));
  G->compiler_name = "turclc";
  G->output_filename = "a.out";
  G->h_bits = 8;
  G->h_minreg = 8;
  G->h_minheap = 16;
  G->h_minstack = 8;
  // Init dynamic globals
  G->strings_capacity = 1024;
  G->strings = solid_malloc(sizeof(G->strings[0]) * G->strings_capacity);
  G->macros_capacity = 256;
  G->macros = solid_malloc(sizeof(G->macros[0]) * G->macros_capacity);
  G->labels_capacity = 256;
  G->labels = solid_malloc(sizeof(G->labels[0]) * G->labels_capacity);
  G->ioffsets_capacity = 256;
  G->ioffsets = solid_malloc(sizeof(G->ioffsets[0]) * G->ioffsets_capacity);
  G->bplist_capacity = 64;
  G->bplist = solid_malloc(sizeof(G->bplist[0]) * G->bplist_capacity);
  seg_init(&G->s_dws, 0);
  seg_init(&G->s_code, 0);
  // After init dynamic globals
  Token_Info nltok;
  memset(&nltok, 0, sizeof(nltok));
  nltok.type = T_NEWLINE;
  add_macro_atom(get_si("_NL"), nltok);
  G->macros_num_active = G->macros_num;
  // Options
  int argi = 0;
  if (argc < 2) {
    print_help();
    exit(EXIT_SUCCESS);
  }
  for (argi = 1; argi < argc; argi++) {
    bool help = false
    || (strcmp(argv[argi], "-help") == 0)
    || (strcmp(argv[argi], "-h") == 0)
    || (strcmp(argv[argi], "--help") == 0)
    || (strcmp(argv[argi], "--h") == 0)
    ;
    bool version = false
    || (strcmp(argv[argi], "-version") == 0)
    || (strcmp(argv[argi], "-v") == 0)
    || (strcmp(argv[argi], "--version") == 0)
    || (strcmp(argv[argi], "--v") == 0)
    ;
    if (strcmp(argv[argi], "-fnoescseq") == 0) {
      G->f_noescseq = true;
    }
    else if (strcmp(argv[argi], "-f_start") == 0) {
      G->f_start = true;
    }
    else if (strcmp(argv[argi], "-pedantic") == 0) {
      G->f_pedantic = true;
    }
    else if (help) {
      print_help();
      exit(EXIT_SUCCESS);
    }
    else if (version) {
      print_version();
      exit(EXIT_SUCCESS);
    }
    else {
      break;
    }
  }
  if (argi >= argc) {
    fprintf(stderr, "%s: ", G->compiler_name);
    fprintf(stderr, "error: no <source>\n");
    exit(EXIT_FAILURE);
  }
  if (argc - argi > 2 || argv[argi][0] == '-') {
    fprintf(stderr, "%s: ", G->compiler_name);
    fprintf(stderr, "error: unknow option \"%s\"\n", argv[argi]);
    exit(EXIT_FAILURE);
  }
  G->source_filename = argv[argi];
  argi += 1;
  if (argi < argc) {
    G->output_filename = argv[argi];
  }
  init_lexer(G->source_filename);
  read_token();
  // Headers
  read_headers();
  // Intepretate code and data
  read_body();
  // add exit(0) so program will exit successfully after the last instruction
  uint8_t exit_syscall[] = {
    0xB8, 0x3C, 0x00, 0x00, 0x00, // mov eax, 0x3C
    0xBF, 0x00, 0x00, 0x00, 0x00, // mov edi, 0
    0x0F, 0x05,                   // syscall
  };
  seg_write(&G->s_code, exit_syscall, sizeof(exit_syscall));
  // Check for undefined labels (except ._start)
  for (int i = 0; i < G->labels_num; i++) {
    Label_Info *lab = &G->labels[i];
    if (lab->is_used && !lab->is_defined) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, lab->usedtok.row, lab->usedtok.col);
      fprintf(stderr, "error: label is used but never defined\n");
      exit(EXIT_FAILURE);
    }
  }
  // Link variables
  int regs_size = G->h_minreg * 8;
  int buff_size = 8;
  G->head_size = SEG_ALIGN;
  G->code_size = align_size(G->s_code.size);
  G->temp_size = align_size(regs_size + buff_size);
  G->data_size = align_size(G->s_dws.size + G->h_minheap * 8);
  G->head_ptr = BASE_ADDR;
  G->code_ptr = G->head_ptr + G->head_size;
  G->temp_ptr = G->code_ptr + G->code_size;
  G->data_ptr = G->temp_ptr + G->temp_size;
  if (G->f_run) {
    uint64_t invalid = (uint64_t)-1;
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    int rw = PROT_READ | PROT_WRITE;
    int rwx = PROT_READ | PROT_EXEC | PROT_WRITE;
    //
    G->code_ptr = (int64_t)mmap(NULL, G->code_size, rwx, flags, -1, 0);
    G->temp_ptr = (int64_t)mmap(NULL, G->temp_size, rw, flags, -1, 0);
    G->data_ptr = (int64_t)mmap(NULL, G->data_size, rw, flags, -1, 0);
    if (G->code_ptr == invalid || G->temp_ptr == invalid || G->data_ptr == invalid) {
      fprintf(stderr, "%s: ", G->compiler_name);
      fprintf(stderr, "error: mmap failed\n");
      exit(EXIT_FAILURE);
    }
  }
  G->entry_ptr = G->code_ptr;
  if (G->f_start) {
    Label_Info *lab = get_label(get_si("_start"));
    if (!lab->is_defined || lab->seg_type != SEG_CODE) {
      fprintf(stderr, "%s: ", G->source_filename);
      fprintf(stderr, "error: \"._start\" is not found in code\n");
      exit(EXIT_FAILURE);
    }
    G->entry_ptr = G->code_ptr + lab->offset;
  }
  // Backpatch
  for (int i = 0; i < G->bplist_num; i++) {
    const Backpatch *bp = &G->bplist[i];
    Segment *seg = (bp->seg_type == SEG_CODE) ? &G->s_code : &G->s_dws;
    // hack segment members
    Segment chunk;
    chunk.size = 0;
    chunk.capacity = 16;
    chunk.bytes = &seg->bytes[bp->offset];
    // write the value
    uint64_t value = bp_cumpute_value(bp);
    if (bp->size == 4) {
      seg_put_le32(&chunk, value);
    }
    else if (bp->size == 8) {
      seg_put_le64(&chunk, value);
    }
  }
  // -run
  if (G->f_run) {
    void *code = (void *)G->code_ptr;
    void *temp = (void *)G->temp_ptr;
    void *data = (void *)G->data_ptr;
    memcpy(code, G->s_code.bytes, G->s_code.size);
    memset(temp, 0, G->temp_size);
    memcpy(data, G->s_dws.bytes, G->s_dws.size);
    memset(data + G->s_dws.size, 0, G->data_size - G->s_dws.size);
    Entry_Func entry = (Entry_Func)G->entry_ptr;
    entry();
    fprintf(stderr, "%s: ", G->source_filename);
    fprintf(stderr, "error: URCL program returned to the parent proccess\n");
    exit(EXIT_FAILURE);
    return 0;
  }
  save_as_elf_executable();
  // OK
  exit(EXIT_SUCCESS);
  return 0;
}

////////////////////////////////////////////////////////////////
uint64_t bp_cumpute_value(const Backpatch *bp)
{
  if (bp->type == BP_LABEL) {
    const Label_Info *lab = get_label(bp->value.label_name_si);
    if (lab->seg_type == SEG_CODE) {
      return G->code_ptr + lab->offset;
    }
    if (lab->seg_type == SEG_DWS) {
      return lab->offset / 8;
    }
  }
  if (bp->type == BP_MEMPTR) {
    int m0 = G->s_dws.size / 8;
    return m0 + bp->value.memptr_index;
  }
  if (bp->type == BP_REGPTR) {
    return G->temp_ptr + bp->value.regptr_index * 8;
  }
  if (bp->type == BP_RELPTR) {
    int i = bp->value.relptr_index;
    Token_Info tok = bp->ctxtok;
    if (i < 0 || i >= G->ioffsets_num) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
      fprintf(stderr, "error: invalid relative address\n");
      exit(EXIT_FAILURE);
    }
    return G->code_ptr + G->ioffsets[i];
  }
  if (bp->type == BP_DATAPTR) {
    return G->data_ptr;
  }
  return 0;
}

////////////////////////////////////////////////////////////////
void save_as_elf_executable(void)
{
  FILE *output_file = fopen(G->output_filename, "wb");
  if (output_file == NULL) {
    fprintf(stderr, "%s: ", G->output_filename);
    fprintf(stderr, "error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  //
  Segment s_elf;
  seg_init(&s_elf, 0x4000);
  // File header
  static const uint8_t ident[] = {
    0x7F, 'E', 'L', 'F', // MAGIC
    2, 1, 1, 0, 0, // 64-bit, LE, v1.0, System V, v0.0
    0, 0, 0, 0, 0, 0, 0, // PAD (7 bytes)
  };
  static const uint8_t start[] = {
    0x02, 0x00, // type = executable
    0x3E, 0x00, // mach = x86-64
    0x01, 0x00, 0x00, 0x00, // ELF v1.0
  };
  seg_write(&s_elf, ident, sizeof(ident));
  seg_write(&s_elf, start, sizeof(start));
  int ph_offset = 64;
  int ph_item_size = 0x38;
  int ph_num = 4; // ROD, CODE, TEMP, DATA
  int ph_size = ph_item_size * ph_num;
  int sh_offset = ph_offset + ph_size;
  int sh_item_size = 64;
  int sh_num = 4; // NAMES, CODE, DATA
  int sh_size = sh_item_size * sh_num;
  int strtab_offset = sh_offset + sh_size;
  const char *strtab_name = ".strtab";
  const char *code_name = ".text";
  const char *data_name = ".data";
  int strtab_name_len = strlen(strtab_name);
  int code_name_len = strlen(code_name);
  int data_name_len = strlen(data_name);
  int strtab_size = 1 + strtab_name_len + 1 + code_name_len + 1 + data_name_len + 1;
  seg_put_le64(&s_elf, G->entry_ptr);
  seg_put_le64(&s_elf, ph_offset);
  seg_put_le64(&s_elf, sh_offset);
  seg_put_le32(&s_elf, 0); // flags
  seg_put_le16(&s_elf, 64); // ELF header size
  seg_put_le16(&s_elf, ph_item_size);
  seg_put_le16(&s_elf, ph_num);
  seg_put_le16(&s_elf, sh_item_size);
  seg_put_le16(&s_elf, sh_num);
  seg_put_le16(&s_elf, 1); // index of section NAMES
  // Program header
  int loadable = 1;
  int ro = 4;
  int rw = 6;
  int rx = 5;
  uint64_t head_offset = 0;
  uint64_t code_offset = head_offset + G->head_size;
  uint64_t temp_offset = code_offset + G->code_size;
  uint64_t data_offset = temp_offset + G->temp_size;
  // HEAD
  seg_put_le32(&s_elf, loadable);
  seg_put_le32(&s_elf, ro);
  seg_put_le64(&s_elf, head_offset);
  seg_put_le64(&s_elf, G->head_ptr);
  seg_put_le64(&s_elf, G->head_ptr);
  seg_put_le64(&s_elf, G->head_size);
  seg_put_le64(&s_elf, G->head_size);
  seg_put_le64(&s_elf, SEG_ALIGN);
  // CODE
  seg_put_le32(&s_elf, loadable);
  seg_put_le32(&s_elf, rx);
  seg_put_le64(&s_elf, code_offset);
  seg_put_le64(&s_elf, G->code_ptr);
  seg_put_le64(&s_elf, G->code_ptr);
  seg_put_le64(&s_elf, G->code_size);
  seg_put_le64(&s_elf, G->code_size);
  seg_put_le64(&s_elf, SEG_ALIGN);
  // TEMP
  seg_put_le32(&s_elf, loadable);
  seg_put_le32(&s_elf, rw);
  seg_put_le64(&s_elf, temp_offset);
  seg_put_le64(&s_elf, G->temp_ptr);
  seg_put_le64(&s_elf, G->temp_ptr);
  seg_put_le64(&s_elf, G->temp_size);
  seg_put_le64(&s_elf, G->temp_size);
  seg_put_le64(&s_elf, SEG_ALIGN);
  // DATA
  seg_put_le32(&s_elf, loadable);
  seg_put_le32(&s_elf, rw);
  seg_put_le64(&s_elf, data_offset);
  seg_put_le64(&s_elf, G->data_ptr);
  seg_put_le64(&s_elf, G->data_ptr);
  seg_put_le64(&s_elf, G->data_size);
  seg_put_le64(&s_elf, G->data_size);
  seg_put_le64(&s_elf, SEG_ALIGN);
  // Section header
  int progbits = 1;
  int strtab = 3;
  int has_strings = 0x20;
  int alloc_data = 3;
  int alloc_code = 6;
  // NULL ""
  seg_put_zeros(&s_elf, sh_item_size);
  // NAMES ".strtab"
  seg_put_le32(&s_elf, 1);
  seg_put_le32(&s_elf, strtab);
  seg_put_le64(&s_elf, has_strings);
  seg_put_le64(&s_elf, 0); // sh_addr
  seg_put_le64(&s_elf, strtab_offset);
  seg_put_le64(&s_elf, strtab_size);
  seg_put_le32(&s_elf, 0);
  seg_put_le32(&s_elf, 0);
  seg_put_le64(&s_elf, SEG_ALIGN);
  seg_put_le64(&s_elf, 0);
  // CODE ".text"
  seg_put_le32(&s_elf, 1 + strtab_name_len + 1);
  seg_put_le32(&s_elf, progbits);
  seg_put_le64(&s_elf, alloc_code);
  seg_put_le64(&s_elf, G->code_ptr);
  seg_put_le64(&s_elf, code_offset);
  seg_put_le64(&s_elf, G->s_code.size);
  seg_put_le32(&s_elf, 0);
  seg_put_le32(&s_elf, 0);
  seg_put_le64(&s_elf, SEG_ALIGN);
  seg_put_le64(&s_elf, 0);
  // DATA ".data"
  seg_put_le32(&s_elf, 1 + strtab_name_len + 1 + code_name_len + 1);
  seg_put_le32(&s_elf, progbits);
  seg_put_le64(&s_elf, alloc_data);
  seg_put_le64(&s_elf, G->data_ptr);
  seg_put_le64(&s_elf, data_offset);
  seg_put_le64(&s_elf, G->s_dws.size);
  seg_put_le32(&s_elf, 0);
  seg_put_le32(&s_elf, 0);
  seg_put_le64(&s_elf, SEG_ALIGN);
  seg_put_le64(&s_elf, 0);
  // Strings
  seg_put_byte(&s_elf, 0); // an empty string
  seg_write(&s_elf, strtab_name, strtab_name_len + 1);
  seg_write(&s_elf, code_name, code_name_len + 1);
  seg_write(&s_elf, data_name, data_name_len + 1);
  // Content
  seg_put_zeros(&s_elf, G->head_size - s_elf.size);
  seg_write(&s_elf, G->s_code.bytes, G->s_code.size);
  seg_put_zeros(&s_elf, G->code_size - G->s_code.size);
  seg_put_zeros(&s_elf, G->temp_size);
  seg_write(&s_elf, G->s_dws.bytes, G->s_dws.size);
  seg_put_zeros(&s_elf, G->data_size - G->s_dws.size);
  // Save
  fwrite(s_elf.bytes, 1, s_elf.size, output_file);
  fclose(output_file);
}

/*
MIT License

Copyright (c) 2024 Artem Pirunov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

