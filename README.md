# Tiny URCL Compiler

## Features

- **Direct compiling!**
No linking or assembly required.

- **Scripting!**
Just add a shebang like `#!/usr/local/bin/turclc -run`
at the first line of your URCL source,
and run it directly from the terminal.
(doesn't work at the moment)

- **URCL Explorer compatible!**
Experiment in your browser, compile as a native executable.
And vice-versa.

- **Extensions!**
The compiler provides several features that go beyond the standard.
Read the compiler documentation and `-help` option.

## Limitations

- The number of `BITS` cannot exceed `64`.

- `RUN RAM` is not supported.

## Links

[URCL Documentation](https://github.com/ModPunchtree/URCL)

[URCL Explorer](https://bramotte.github.io/urcl-explorer)

## Build and Install

```
cc -o turclc buildme.c
mv turclc /usr/local/bin/turclc
```

## License

See LICENSE file for the license information
