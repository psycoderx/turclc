// See end of file for the license information

//   turclc_parser.c
// Parse source and generate output

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#include "turclc.h"

////////////////////////////////////////////////////////////////
// IMPLEMENTATION
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
const Instr *get_instruction_info(int tok_type)
{
  static const Instr itable[] = {
    { .type = T_ADD, .len = 4, .args = "RNN", .prep = P_YTOA, .func = i_binary },
    { .type = T_SUB, .len = 4, .args = "RNN", .prep = P_YTOA, .func = i_binary },
    { .type = T_NOR, .len = 4, .args = "RNN", .prep = P_YTOA, .func = i_binary },
    { .type = T_RSH, .len = 3, .args = "RN ", .prep = P_YTOA, .func = i_unary },
    { .type = T_LOD, .len = 3, .args = "RX ", .prep = P_NO,   .func = i_unary },
    { .type = T_STR, .len = 3, .args = "XN ", .prep = P_NO,   .func = i_unary },
    { .type = T_IMM, .len = 3, .args = "RI ", .prep = P_NO,   .func = i_unary },
    { .type = T_MOV, .len = 3, .args = "RN ", .prep = P_NO,   .func = i_unary },
    { .type = T_IN,  .len = 3, .args = "RP ", .prep = P_NO,   .func = i_input },
    { .type = T_OUT, .len = 3, .args = "PN ", .prep = P_NO,   .func = i_output },
    { .type = T_HLT, .len = 1, .args = "   ", .prep = P_NO,   .func = i_special },
    { .type = T_NOP, .len = 1, .args = "   ", .prep = P_NO,   .func = i_special },
    { .type = T_JMP, .len = 2, .args = "J  ", .prep = P_NO,   .func = i_jump },
    { .type = T_BGE, .len = 4, .args = "JNN", .prep = P_CMPZ, .func = i_jump },
    { .type = T_BLE, .len = 4, .args = "JNN", .prep = P_CMPZ, .func = i_jump },
    { .type = T_BRG, .len = 4, .args = "JNN", .prep = P_CMPZ, .func = i_jump },
    { .type = T_BRL, .len = 4, .args = "JNN", .prep = P_CMPZ, .func = i_jump },
  };
  int itable_num = sizeof(itable) / sizeof(itable[0]);
  for (int i = 0; i < itable_num; i++) {
    if (itable[i].type == tok_type) {
      return &itable[i];
    }
  }
  // unreachable
  return NULL;
}

////////////////////////////////////////////////////////////////
void i_binary(const Instr *me)
{
  gen_arg_as_number(G->atok, TREG_RAX);
  gen_arg_as_number(G->btok, TREG_RBX);
  if (me->type == T_ADD) {
    uint8_t add_rax_rbx[] = { 0x48, 0x01, 0xD8 };
    seg_write(&G->s_code, add_rax_rbx, sizeof(add_rax_rbx));
  }
  else if (me->type == T_SUB) {
    uint8_t sub_rax_rbx[] = { 0x48, 0x29, 0xD8 };
    seg_write(&G->s_code, sub_rax_rbx, sizeof(sub_rax_rbx));
  }
  else if (me->type == T_NOR) {
    uint8_t nor_rax_rbx[] = { 0x48, 0x09, 0xD8, 0x48, 0xF7, 0xD0 };
    seg_write(&G->s_code, nor_rax_rbx, sizeof(nor_rax_rbx));
  }
  else {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
    fprintf(stderr, "bug: not implemented\n");
    exit(EXIT_FAILURE);
  }
  gen_treg_to_reg(G->ytok.num - 1, TREG_RAX);
}

////////////////////////////////////////////////////////////////
void i_input(const Instr *me)
{
  if (G->atok.num != PORT_TEXT) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->atok.row, G->atok.col);
    fprintf(stderr, "error: this port is not implemented\n");
    exit(EXIT_FAILURE);
  }
  // mov rsi, ptr64
  uint8_t mov_rsi_imm64[] = {
    0x48, 0xBE
  };
  seg_write(&G->s_code, mov_rsi_imm64, sizeof(mov_rsi_imm64));
  Backpatch *bp = add_bp(BP_REGPTR, SEG_CODE, G->s_code.size, 8);
  bp->value.regptr_index = G->ytok.num - 1;
  seg_put_le64(&G->s_code, 0);
  // read
  uint8_t read_syscall[] = {
    0xB8, 0x00, 0x00, 0x00, 0x00, // mov rax, 0
    0xBF, 0x00, 0x00, 0x00, 0x00, // mov rdi, 0
    0xBA, 0x01, 0x00, 0x00, 0x00, // mov rdx, 1
    0x0F, 0x05,                   // syscall
  };
  seg_write(&G->s_code, read_syscall, sizeof(read_syscall));
}

////////////////////////////////////////////////////////////////
void i_jump(const Instr *me)
{
  // jmp rcx
  uint8_t jmp_rcx[] = { 0xFF, 0xE1 };
  if (me->type == T_JMP) {
    gen_arg_as_number(G->ytok, TREG_RCX);
    seg_write(&G->s_code, jmp_rcx, sizeof(jmp_rcx));
    return;
  }
  gen_arg_as_number(G->atok, TREG_RAX);
  gen_arg_as_number(G->btok, TREG_RBX);
  gen_arg_as_number(G->ytok, TREG_RCX);
  // cmp rax, rbx
  uint8_t cmp_rax_rbx[] = { 0x48, 0x39, 0xD8 };
  seg_write(&G->s_code, cmp_rax_rbx, sizeof(cmp_rax_rbx));
  if (me->type == T_BGE) {
    seg_put_byte(&G->s_code, 0x72); // JB
  }
  else if (me->type == T_BLE) {
    seg_put_byte(&G->s_code, 0x77); // JA
  }
  else if (me->type == T_BRG) {
    seg_put_byte(&G->s_code, 0x76); // JBE
  }
  else if (me->type == T_BRL) {
    seg_put_byte(&G->s_code, 0x73); // JAE
  }
  else if (me->type == T_BRE) {
    seg_put_byte(&G->s_code, 0x75); // JNE
  }
  else if (me->type == T_BNE) {
    seg_put_byte(&G->s_code, 0x74); // JE
  }
  else {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
    fprintf(stderr, "bug: not implemented\n");
    exit(EXIT_FAILURE);
  }
  seg_put_byte(&G->s_code, sizeof(jmp_rcx));
  seg_write(&G->s_code, jmp_rcx, sizeof(jmp_rcx));
}

////////////////////////////////////////////////////////////////
void i_output(const Instr *me)
{
  if (G->ytok.num != PORT_TEXT) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->ytok.row, G->ytok.col);
    fprintf(stderr, "error: this port is not implemented\n");
    exit(EXIT_FAILURE);
  }
  gen_arg_as_number(G->atok, TREG_RAX);
  int buffi = G->h_minreg;
  gen_treg_to_reg(buffi, TREG_RAX);
  // mov rsi, ptr64
  uint8_t mov_rsi_imm64[] = {
    0x48, 0xBE
  };
  seg_write(&G->s_code, mov_rsi_imm64, sizeof(mov_rsi_imm64));
  Backpatch *bp = add_bp(BP_REGPTR, SEG_CODE, G->s_code.size, 8);
  bp->value.regptr_index = buffi;
  seg_put_le64(&G->s_code, 0);
  // write
  uint8_t write_syscall[] = {
    0xB8, 0x01, 0x00, 0x00, 0x00, // mov rax, 1
    0xBF, 0x01, 0x00, 0x00, 0x00, // mov rdi, 1
    0xBA, 0x01, 0x00, 0x00, 0x00, // mov rdx, 1
    0x0F, 0x05,                   // syscall
  };
  seg_write(&G->s_code, write_syscall, sizeof(write_syscall));
}

////////////////////////////////////////////////////////////////
void i_special(const Instr *me)
{
  if (me->type == T_NOP) {
    seg_put_byte(&G->s_code, 0x90);
    return;
  }
  if (me->type == T_HLT) {
    uint8_t exit_syscall[] = {
      0xB8, 0x3C, 0x00, 0x00, 0x00, // mov eax, 0x3C
      0xBF, 0x00, 0x00, 0x00, 0x00, // mov edi, 0
      0x0F, 0x05,                   // syscall
    };
    seg_write(&G->s_code, exit_syscall, sizeof(exit_syscall));
    return;
  }
  fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
  fprintf(stderr, "bug: not implemented\n");
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////
void i_unary(const Instr *me)
{
  if (me->args[1] == 'N' || me->args[1] == 'I') {
    gen_arg_as_number(G->atok, TREG_RAX);
  }
  else if (me->args[1] == 'X') {
    gen_arg_as_load(G->atok, TREG_RAX);
  }
  if (me->type == T_RSH) {
    uint8_t shr_rax[] = { 0x48, 0xD1, 0xE8 };
    seg_write(&G->s_code, shr_rax, sizeof(shr_rax));
  }
  if (me->args[0] == 'R') {
    gen_treg_to_reg(G->ytok.num - 1, TREG_RAX);
  }
  else if (me->args[0] == 'X') {
    gen_treg_to_mem(G->ytok, TREG_RAX);
  }
}

////////////////////////////////////////////////////////////////
void gen_arg_as_load(Token_Info tok, int treg)
{
  // mov reg, [ptr32 + rcx * 8]
  uint8_t mov_reg_mem[] = {
    0x48, 0x8B, 0x04, 0xCD,
    0x48, 0x8B, 0x0C, 0xCD,
    0x48, 0x8B, 0x14, 0xCD,
    0x48, 0x8B, 0x1C, 0xCD,
  };
  gen_arg_as_number(tok, TREG_RCX);
  seg_write(&G->s_code, &mov_reg_mem[treg * 4], 4);
  Backpatch *bp = add_bp(BP_DATAPTR, SEG_CODE, G->s_code.size, 4);
  bp->ctxtok = tok;
  seg_put_le32(&G->s_code, 0); // FIXME: use 64 bit address somehow
}

////////////////////////////////////////////////////////////////
void gen_treg_to_mem(Token_Info tok, int treg)
{
  // mov [ptr32 + rcx * 8], reg
  uint8_t mov_mem_reg[] = {
    0x48, 0x89, 0x04, 0xCD,
    0x48, 0x89, 0x0C, 0xCD,
    0x48, 0x89, 0x14, 0xCD,
    0x48, 0x89, 0x1C, 0xCD,
  };
  gen_arg_as_number(tok, TREG_RCX);
  seg_write(&G->s_code, &mov_mem_reg[treg * 4], 4);
  Backpatch *bp = add_bp(BP_DATAPTR, SEG_CODE, G->s_code.size, 4);
  bp->ctxtok = tok;
  seg_put_le32(&G->s_code, 0); // FIXME: use 64 bit address somehow
}

////////////////////////////////////////////////////////////////
void gen_treg_to_reg(int regi, int treg)
{
  // mov [ptr32], reg
  uint8_t mov_at_ptr32_reg[] = {
    0x48, 0x89, 0x04, 0x25,
    0x48, 0x89, 0x0C, 0x25,
    0x48, 0x89, 0x14, 0x25,
    0x48, 0x89, 0x1C, 0x25,
  };
  seg_write(&G->s_code, &mov_at_ptr32_reg[treg * 4], 4);
  Backpatch *bp = add_bp(BP_REGPTR, SEG_CODE, G->s_code.size, 4);
  bp->value.regptr_index = regi;
  seg_put_le32(&G->s_code, 0); // FIXME: use 64 bit address somehow
}

////////////////////////////////////////////////////////////////
void gen_arg_as_number(Token_Info tok, int treg)
{
  // mov reg, [ptr32]
  if (tok.type == T_REGPTR) {
    uint8_t mov_reg_at_ptr32[] = {
      0x48, 0x8B, 0x04, 0x25,
      0x48, 0x8B, 0x0C, 0x25,
      0x48, 0x8B, 0x14, 0x25,
      0x48, 0x8B, 0x1C, 0x25,
    };
    seg_write(&G->s_code, &mov_reg_at_ptr32[treg * 4], 4);
    Backpatch *bp = add_bp(BP_REGPTR, SEG_CODE, G->s_code.size, 4);
    bp->ctxtok = tok;
    bp->value.regptr_index = tok.num - 1;
    seg_put_le32(&G->s_code, 0); // FIXME: use 64 bit address somehow
    return;
  }
  // mov reg, imm64
  seg_put_byte(&G->s_code, 0x48);
  seg_put_byte(&G->s_code, 0xB8 + treg);
  if (tok.type == T_INTLIT) {
    seg_put_le64(&G->s_code, tok.num);
  }
  else if (tok.type == T_MEMPTR) {
    Backpatch *bp = add_bp(BP_MEMPTR, SEG_CODE, G->s_code.size, 8);
    bp->ctxtok = tok;
    bp->value.memptr_index = tok.num;
    seg_put_le64(&G->s_code, 0);
  }
  else if (tok.type == T_LABEL) {
    Backpatch *bp = add_bp(BP_LABEL, SEG_CODE, G->s_code.size, 8);
    bp->ctxtok = tok;
    bp->value.label_name_si = tok.si;
    seg_put_le64(&G->s_code, 0);
  }
  else if (tok.type == T_RELPTR) {
    Backpatch *bp = add_bp(BP_RELPTR, SEG_CODE, G->s_code.size, 8);
    bp->ctxtok = tok;
    bp->value.relptr_index = tok.num;
    seg_put_le64(&G->s_code, 0);
  }
}

////////////////////////////////////////////////////////////////
void read_body(void)
{
  static bool pedantic_args_altnum = false;
start:
  next_code_line();
  if (G->tok.type == T_EOF) {
    return;
  }
  // DW x or DW [ xs... ]
  if (G->tok.type == T_DW) {
    define_declared_labels(SEG_DWS, G->s_dws.size);
    read_token();
    if (G->tok.type != T_BRACK_BEG) {
      compile_dw_value();
      read_token();
      expect_token(T_NEWLINE, "a newline");
      read_token();
      goto start;
    }
    read_token();
    bool pedantic_multiline_dw = false;
    while (G->tok.type != T_BRACK_END && G->tok.type != T_EOF) {
      if (G->tok.type == T_NEWLINE) {
        if (G->f_pedantic && !pedantic_multiline_dw) {
          fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
          fprintf(stderr, "pedantic: multiline DW\n");
          pedantic_multiline_dw = true;
        }
        read_token();
        continue;
      }
      compile_dw_value();
      read_token();
    }
    if (G->tok.type == T_EOF) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: missing ending bracket\n");
      exit(EXIT_FAILURE);
    }
    read_token(); // skip ]
    expect_token(T_NEWLINE, "a newline");
    read_token();
    goto start;
  }
  // Label declaration
  if (G->tok.type == T_LABEL) {
    Label_Info *lab = get_label(G->tok.si);
    if (lab->is_declared) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: label redefinition\n");
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, lab->decltok.row, lab->decltok.col);
      fprintf(stderr, "note: the label first defined here\n");
      exit(EXIT_FAILURE);
    }
    lab->decltok = G->tok;
    lab->is_declared = true;
    read_token();
    expect_token(T_NEWLINE, "a newline");
    read_token();
    goto start;
  }
  // Instruction
  define_declared_labels(SEG_CODE, G->s_code.size);
  add_instruction_offset(G->s_code.size);
  Token_Info stmt[STMT_MAXLEN];
  int stmt_len = 0;
  while (stmt_len < STMT_MAXLEN && G->tok.type != T_NEWLINE && G->tok.type != T_EOF) {
    stmt[stmt_len] = G->tok;
    stmt_len += 1;
    read_token();
  }
  expect_token(T_NEWLINE, "a newline");
  read_token();
  G->itok = stmt[0];
  G->ytok = stmt[1];
  G->atok = stmt[2];
  G->btok = stmt[3];
  if (!is_instruction(G->itok.type)) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
    fprintf(stderr, "error: an instruction is expected but got something else\n");
    exit(EXIT_FAILURE);
  }
  const Instr *instr = get_instruction_info(G->itok.type);
  if (stmt_len < instr->len && instr->prep != P_NO) {
    if (!pedantic_args_altnum && G->f_pedantic) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
      fprintf(stderr, "pedantic: alternative number of arguments\n");
      pedantic_args_altnum = true;
    }
    if (instr->prep == P_YTOA) {
      G->btok = G->atok;
      G->atok = G->ytok;
      stmt_len += 1;
    }
    else if (instr->prep == P_CMPZ) {
      G->btok = G->atok;
      G->atok.type = T_INTLIT;
      G->atok.num = 0;
      G->atok.si = 0;
      stmt_len += 1;
    }
  }
  if (stmt_len < instr->len) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
    fprintf(stderr, "error: not enough arguments\n");
    exit(EXIT_FAILURE);
  }
  if (stmt_len > instr->len) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->itok.row, G->itok.col);
    fprintf(stderr, "error: too many arguments\n");
    exit(EXIT_FAILURE);
  }
  // Type checking
  check_type(G->ytok, instr->args[0]);
  check_type(G->atok, instr->args[1]);
  check_type(G->btok, instr->args[2]);
  // Generate code
  instr->func(instr);
  goto start;
}

////////////////////////////////////////////////////////////////
void check_type(Token_Info tok, char type)
{
  if (type == ' ') {
    return;
  }
  if (type == 'R') {
    if (tok.type == T_REGPTR) {
      return;
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: it is not a register\n");
    exit(EXIT_FAILURE);
  }
  if (type == 'N') {
    if (tok.type == T_REGPTR || tok.type == T_INTLIT || tok.type == T_MEMPTR) {
      return;
    }
    if (tok.type == T_LABEL || tok.type == T_RELPTR) {
      return;
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: it is not a register, constant, or address\n");
    exit(EXIT_FAILURE);
  }
  if (type == 'X') {
    if (tok.type == T_REGPTR || tok.type == T_INTLIT || tok.type == T_MEMPTR) {
      return;
    }
    if (tok.type == T_LABEL) {
      const Label_Info *lab = get_label(tok.si);
      if (lab->seg_type == SEG_DWS || lab->seg_type == SEG_UNKNOWN) {
        return;
      }
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: cannot use this as a memory address\n");
    exit(EXIT_FAILURE);
  }
  if (type == 'I') {
    if (tok.type == T_INTLIT || tok.type == T_MEMPTR) {
      return;
    }
    if (tok.type == T_LABEL || tok.type == T_RELPTR) {
      return;
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: it is not a constant or address\n");
    exit(EXIT_FAILURE);
  }
  if (type == 'P') {
    if (tok.type == T_PORT) {
      return;
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: it is not a port name\n");
    exit(EXIT_FAILURE);
  }
  if (type == 'J') {
    if (tok.type == T_REGPTR) {
      return;
    }
    if (tok.type == T_LABEL) {
      const Label_Info *lab = get_label(tok.si);
      if (lab->seg_type == SEG_CODE || lab->seg_type == SEG_UNKNOWN) {
        return;
      }
    }
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
    fprintf(stderr, "error: cannot use this as an instruction address\n");
    exit(EXIT_FAILURE);
  }
  fprintf(stderr, "%s:%i:%i: ", G->source_filename, tok.row, tok.col);
  fprintf(stderr, "bug: the type system got an invalid type\n");
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////
void compile_dw_value(void)
{
  if (G->tok.type == T_INTLIT) {
    seg_put_le64(&G->s_dws, G->tok.num);
    return;
  }
  if (G->tok.type == T_STRLIT) {
    const char *str = get_str(G->tok.si);
    for (int i = 0; str[i] != '\0'; i++) {
      seg_put_le64(&G->s_dws, str[i]);
    }
    return;
  }
  if (G->tok.type == T_LABEL) {
    Label_Info *lab = get_label(G->tok.si);
    if (!lab->is_used) {
      lab->usedtok = G->tok;
    }
    lab->is_used = true;
    if (lab->is_defined && lab->seg_type == SEG_DWS) {
      seg_put_le64(&G->s_dws, lab->offset / 8);
      return;
    }
    Backpatch *bp = add_bp(BP_LABEL, SEG_DWS, G->s_dws.size, 8);
    bp->ctxtok = G->tok;
    bp->value.label_name_si = G->tok.si;
    seg_put_le64(&G->s_dws, 0);
    return;
  }
  if (G->tok.type == T_MEMPTR) {
    Backpatch *bp = add_bp(BP_MEMPTR, SEG_DWS, G->s_dws.size, 8);
    bp->ctxtok = G->tok;
    bp->value.memptr_index = G->tok.num;
    seg_put_le64(&G->s_dws, 0);
    return;
  }
  fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
  fprintf(stderr, "error: not a DW value\n");
  exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////
void define_declared_labels(int seg_type, int offset)
{
  for (int i = 0; i < G->labels_num; i++) {
    Label_Info *lab = &G->labels[i];
    if (lab->is_declared && !lab->is_defined) {
      lab->is_defined = true;
      lab->seg_type = seg_type;
      lab->offset = offset;
    }
  }
}

////////////////////////////////////////////////////////////////
void read_headers(void)
{
  struct { int type; int *value; } cvars[] = {
    { .type = T_MINHEAP, .value = &G->h_minheap },
    { .type = T_MINREG, .value = &G->h_minreg },
    { .type = T_MINSTACK, .value = &G->h_minstack },
  };
  int cvars_num = sizeof(cvars) / sizeof(cvars[0]);
start:
  //
  next_code_line();
  // BITS
  if (G->tok.type == T_BITS) {
    read_token();
    bool more_or_equal = false;
    if (G->tok.type == T_EQUAL || G->tok.type == T_MORE || G->tok.type == T_LESS) {
      more_or_equal = (G->tok.type == T_MORE);
      read_token();
    }
    expect_token(T_INTLIT, "an integer");
    if (G->tok.num > 64) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: BITS > 64 is not supported\n");
      exit(EXIT_FAILURE);
    }
    if (G->tok.num < 2) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: invalid value\n");
      exit(EXIT_FAILURE);
    }
    G->h_bits = (more_or_equal) ? 64 : (int)G->tok.num;
    read_token();
    expect_token(T_NEWLINE, "a newline");
    read_token();
    goto start;
  }
  // RUN
  if (G->tok.type == T_RUN) {
    read_token();
    if (G->tok.type != T_RAM && G->tok.type != T_ROM) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: should be RAM or ROM\n");
      exit(EXIT_FAILURE);
    }
    if (G->tok.type != T_RAM) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: RUN RAM is not supported\n");
      exit(EXIT_FAILURE);
    }
    read_token();
    expect_token(T_NEWLINE, "a newline");
    read_token();
    goto start;
  }
  // Simple pairs NAME + INTLIT
  for (int i = 0; i < cvars_num; i++) {
    if (G->tok.type == cvars[i].type) {
      read_token();
      expect_token(T_INTLIT, "an integer");
      if (G->tok.num < 0) {
        fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
        fprintf(stderr, "error: invalid value\n");
        exit(EXIT_FAILURE);
      }
      *(cvars[i].value) = (int)G->tok.num;
      read_token();
      expect_token(T_NEWLINE, "a newline");
      read_token();
      goto start;
    }
  }
}

////////////////////////////////////////////////////////////////
void next_code_line(void)
{
start:
  // Skip newlines
  while (G->tok.type == T_NEWLINE) {
    read_token();
  }
  // Define macro
  if (G->tok.type != T_DEFINE) {
    return;
  }
  read_token();
  // TODO: define by any token, not just T_ID
  expect_token(T_ID, "a macro name");
  int name_si = G->tok.si;
  read_token();
  int n = 0;
  while (G->tok.type != T_NEWLINE && G->tok.type != T_EOF && G->tok.type != T_DEFINE) {
    add_macro_atom(name_si, G->tok);
    n += 1;
    if (n == 2 && G->f_pedantic) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "pedantic: more than one token in a macro\n");
    }
    read_token();
  }
  if (n == 0) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
    fprintf(stderr, "error: an empty macro\n");
    exit(EXIT_FAILURE);
  }
  G->macros_num_active = G->macros_num;
  expect_token(T_NEWLINE, "a newline");
  read_token();
  goto start;
}

////////////////////////////////////////////////////////////////
Backpatch *add_bp(int bp_type, int seg_type, int offset, int size)
{
  int bpi = G->bplist_num;
  G->bplist_num += 1;
  if (G->bplist_num > G->bplist_capacity) {
    G->bplist_capacity = G->bplist_num * 2;
    G->bplist = solid_realloc(G->bplist, G->bplist_capacity * sizeof(G->bplist[0]));
  }
  Backpatch *bp = &G->bplist[bpi];
  memset(bp, 0, sizeof(*bp));
  bp->type = bp_type;
  bp->seg_type = seg_type;
  bp->offset = offset;
  bp->size = size;
  return bp;
}

////////////////////////////////////////////////////////////////
void add_macro_atom(int name_si, Token_Info tok)
{
  Macro_Atom atom;
  memset(&atom, 0, sizeof(atom));
  atom.name_si = name_si;
  atom.tok = tok;
  int atomi = G->macros_num;
  G->macros_num += 1;
  if (G->macros_num > G->macros_capacity) {
    G->macros_capacity = G->macros_num * 2;
    int newsize = G->macros_capacity * sizeof(G->macros[0]);
    G->macros = solid_realloc(G->macros, newsize);
  }
  G->macros[atomi] = atom;
}

////////////////////////////////////////////////////////////////
Label_Info *get_label(int name_si)
{
  for (int i = 0; i < G->labels_num; i++) {
    if (G->labels[i].name_si == name_si) {
      return &G->labels[i];
    }
  }
  int li = G->labels_num;
  G->labels_num += 1;
  if (G->labels_num > G->labels_capacity) {
    G->labels_capacity = G->labels_num * 2;
    G->labels = solid_realloc(G->labels, G->labels_capacity * sizeof(G->labels[0]));
  }
  Label_Info *lab = &G->labels[li];
  memset(lab, 0, sizeof(*lab));
  lab->name_si = name_si;
  return lab;
}

////////////////////////////////////////////////////////////////
void add_instruction_offset(int ioffset)
{
  int ii = G->ioffsets_num;
  G->ioffsets_num += 1;
  if (G->ioffsets_num > G->ioffsets_capacity) {
    G->ioffsets_capacity = G->ioffsets_num * 2;
    G->ioffsets = solid_realloc(G->ioffsets, G->ioffsets_capacity * sizeof(G->ioffsets[0]));
  }
  G->ioffsets[ii] = ioffset;
}

////////////////////////////////////////////////////////////////
void expect_token(int type, const char *name)
{
  if (G->tok.type == type) {
    return;
  }
  fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
  fprintf(stderr, "error: %s is expected but got something else\n", name);
  exit(EXIT_FAILURE);
}

/*
MIT License

Copyright (c) 2024 Artem Pirunov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

