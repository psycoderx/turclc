// See end of file for the license information

//   turclc_core.c
// Common functions

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#include "turclc.h"

////////////////////////////////////////////////////////////////
// IMPLEMENTATION
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
uint64_t align_size(uint64_t size)
{
  int rem = size % SEG_ALIGN;
  if (rem == 0) {
    return size;
  }
  return size + (SEG_ALIGN - rem);
}

////////////////////////////////////////////////////////////////
int64_t get_port_type(int name_si)
{
// TODO: implement ports
// %NUMB - fscanf("%i") fprintf("%i")
// %X and %Y - set/get pixel X and Y position
// %COLOR or %COLOUR get/set color at %X, %Y
// %BUFFER - controls buffers
// %FREEZE - lock the visable buffer
// %UNFREEZE - unlock the visable buffer
// %CLEAR - clear the screen
// %ADDR and %BUS - get/set address and get/set at address
// %RNG - rand() and srand()
// %NOTE - get/set sound device pitch
// %NLEG - play a sound N*0.1 seconds
// %WAIT - wait N*0.1 seconds
// %ASCII8 %CHAR5 %CHAR6 %ASCII7 %UTF8 - %TEXT aliases
// %INT %UINT %BIN %HEX %FLOAT %FIXED - %NUMB aliases
  const char *name = get_str(name_si);
  if (strcmp_icase(name, "TEXT") == 0) {
    return PORT_TEXT;
  }
  if (strcmp_icase(name, "ADDR") == 0) {
    return PORT_ADDR;
  }
  if (strcmp_icase(name, "BUS") == 0) {
    return PORT_BUS;
  }
  return PORT_UNKNOW;
}

////////////////////////////////////////////////////////////////
int get_si(const char *str)
{
  for (int i = 0; i < G->strings_num; i++) {
    if (strcmp(str, G->strings[i]) == 0) {
      return i;
    }
  }
  int si = G->strings_num;
  G->strings_num += 1;
  if (G->strings_num > G->strings_capacity) {
    G->strings_capacity = G->strings_num * 2;
    int newsize = G->strings_capacity * sizeof(G->strings[0]);
    G->strings = solid_realloc(G->strings, newsize);
  }
  int size = strlen(str) + 1;
  char *ptr = solid_malloc(size);
  memcpy(ptr, str, size);
  G->strings[si] = ptr;
  return si;
}

////////////////////////////////////////////////////////////////
const char *get_str(int si)
{
  if (si < 0 || si >= G->strings_num) {
    fprintf(stderr, "%s: ", G->compiler_name);
    fprintf(stderr, "bug: invalid string index\n");
    exit(EXIT_FAILURE);
  }
  return G->strings[si];
}

////////////////////////////////////////////////////////////////
void *solid_malloc(size_t size)
{
  void *ptr = malloc(size);
  if (ptr == NULL) {
    fprintf(stderr, "%s: ", G->compiler_name);
    fprintf(stderr, "error: malloc returned NULL\n");
    exit(EXIT_FAILURE);
  }
  return ptr;
}

////////////////////////////////////////////////////////////////
void *solid_malloc_zeros(size_t size)
{
  void *ptr = solid_malloc(size);
  memset(ptr, 0, size);
  return ptr;
}

////////////////////////////////////////////////////////////////
void *solid_realloc(void *ptr, size_t newsize)
{
  void *newptr = realloc(ptr, newsize);
  if (newptr == NULL) {
    fprintf(stderr, "%s: ", G->compiler_name);
    fprintf(stderr, "error: realloc returned NULL\n");
    exit(EXIT_FAILURE);
  }
  return newptr;
}

////////////////////////////////////////////////////////////////
void seg_init(Segment *seg, int capacity)
{
  seg->size = 0;
  seg->capacity = capacity;
  if (seg->capacity < 256) {
    seg->capacity = 256;
  }
  seg->bytes = solid_malloc(seg->capacity);
}

////////////////////////////////////////////////////////////////
void seg_put_phentry(Segment *seg, int type, int prot, int offset, int size)
{
  
}

////////////////////////////////////////////////////////////////
void seg_put_byte(Segment *seg, uint8_t byte)
{
  seg_write(seg, &byte, 1);
}

////////////////////////////////////////////////////////////////
void seg_put_le16(Segment *seg, uint64_t data)
{
  seg_put_byte(seg, (data >> (8 * 0)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 1)) & 0xFF);
}

////////////////////////////////////////////////////////////////
void seg_put_le32(Segment *seg, uint64_t data)
{
  seg_put_byte(seg, (data >> (8 * 0)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 1)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 2)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 3)) & 0xFF);
}

////////////////////////////////////////////////////////////////
void seg_put_le64(Segment *seg, uint64_t data)
{
  seg_put_byte(seg, (data >> (8 * 0)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 1)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 2)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 3)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 4)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 5)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 6)) & 0xFF);
  seg_put_byte(seg, (data >> (8 * 7)) & 0xFF);
}

////////////////////////////////////////////////////////////////
void seg_write(Segment *seg, const void *bytes, int size)
{
  int newsize = seg->size + size;
  if (newsize > seg->capacity) {
    seg->capacity = newsize * 2;
    seg->bytes = solid_realloc(seg->bytes, seg->capacity);
  }
  memcpy(seg->bytes + seg->size, bytes, size);
  seg->size = newsize;
}

////////////////////////////////////////////////////////////////
void seg_put_zeros(Segment *seg, int count)
{
  for (int i = 0; i < count; i++) {
    seg_put_byte(seg, 0);
  }
}

////////////////////////////////////////////////////////////////
int strcmp_icase(const char *s1, const char *s2)
{
  for (int i = 0; ; i++) {
    int c1 = toupper(s1[i]);
    int c2 = toupper(s2[i]);
    if (c1 != c2) {
      return c1 - c2;
    }
    if (c1 == '\0') {
      break;
    }
  }
  return 0;
}

////////////////////////////////////////////////////////////////
void print_help(void)
{
  static const char *help_msg =
  "Usage:\n"
  "  turclc [options] <source> [output]\n"
  "Options:\n"
  //"  -run        Compile source and run it\n"
  "  -help       Print this information\n"
  "  -version    Print the version information\n"
  "  -fnoescseq  Do not evaluate escape sequences\n"
  "  -f_start    Use \"._start\" as an entry point\n"
  "  -pedantic   Warn about the use of non-standard features\n"
  "\n"
  ;
  printf("%s", help_msg);
}

////////////////////////////////////////////////////////////////
void print_version(void)
{
  static const char *version_msg =
  "turclc A0\n"
  "Copyright (c) 2024 Artem Pirunov\n"
  "This is free software. See the source for copying conditions.\n"
  "\n"
  ;
  printf("%s", version_msg);
}

/*
MIT License

Copyright (c) 2024 Artem Pirunov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

