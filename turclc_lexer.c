// See end of file for the license information

//   urcl_lexer.c
// Read source file characters and tokens

////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////

#include "turclc.h"

////////////////////////////////////////////////////////////////
// IMPLEMENTATION
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
void init_lexer(const char *source_filename)
{
  G->source_filename = source_filename;
  G->source_file = fopen(G->source_filename, "rb");
  if (G->source_file == NULL) {
    fprintf(stderr, "%s: ", G->source_filename);
    fprintf(stderr, "error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  G->chr = '\n';
  read_char();
}

////////////////////////////////////////////////////////////////
int read_token(void)
{
  // TODO: replace T_SEMICOLON with T_NEWLINE
  static bool nl_warned = false;
  static int m_beg = -1;
  static int m_end = -1;
  if (m_beg == m_end) {
    read_token_join();
    if (G->tok.type != T_ID) {
      return G->tok.type;
    }
    m_beg = -1;
    m_end = -1;
    for (int i = 0; i < G->macros_num_active; i++) {
      if (G->macros[i].name_si == G->tok.si) {
        if (m_beg == -1) {
          m_beg = i;
        }
        m_end = i;
      }
    }
    if (m_beg == -1) {
      return G->tok.type;
    }
    m_end += 1;
  }
  if (!nl_warned && G->tok.si == get_si("_NL")) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
    fprintf(stderr, "pedantic: inserted newline\n");
    nl_warned = true;
  }
  G->tok.type = G->macros[m_beg].tok.type;
  G->tok.si = G->macros[m_beg].tok.si;
  G->tok.num = G->macros[m_beg].tok.num;
  m_beg += 1;
  return G->tok.type;
}

////////////////////////////////////////////////////////////////
int read_token_join(void)
{
  static bool nl_escaped = false;
  read_token_solid();
  // Escape a newline
  if (G->tok.type == T_BACKSLASH) {
    Token_Info bstok = G->tok;
    read_token_solid();
    expect_token(T_NEWLINE, "a newline");
    read_token_solid();
    if (G->f_pedantic && !nl_escaped) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, bstok.row, bstok.col);
      fprintf(stderr, "pedantic: escaped newline\n");
      nl_escaped = true;
    }
    return G->tok.type;
  }
  // Label
  if (G->tok.type == T_DOT) {
    Token_Info tok = G->tok;
    read_token_solid();
    expect_token(T_ID, "a label name");
    tok.type = T_LABEL;
    tok.si = G->tok.si;
    G->tok = tok;
    return G->tok.type;
  }
  // Port
  if (G->tok.type == T_PERCENT) {
    Token_Info tok = G->tok;
    read_token_solid();
    expect_token(T_ID, "a port name");
    tok.type = T_PORT;
    tok.num = get_port_type(G->tok.si);
    if (tok.num == PORT_UNKNOW) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: invalid port name\n");
      exit(EXIT_FAILURE);
    }
    G->tok = tok;
    return G->tok.type;
  }
  // Compiler variable
  if (G->tok.type == T_AT) {
    Token_Info tok = G->tok;
    read_token_solid();
    if (G->tok.type == T_DEFINE) {
      tok.type = T_DEFINE;
      G->tok = tok;
      return G->tok.type;
    }
    if (!is_cvariable(G->tok.type)) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: a variable name is expected but got something else\n");
      exit(EXIT_FAILURE);
    }
    tok.type = T_INTLIT;
    tok.num = get_compiler_variable(G->tok.type);
    G->tok = tok;
    return G->tok.type;
  }
  // Relative pointer
  if (G->tok.type == T_TILDE) {
    Token_Info tok = G->tok;
    read_token_solid();
    expect_token(T_INTLIT, "an integer");
    tok.type = T_RELPTR;
    tok.num = G->tok.num;
    G->tok = tok;
    return G->tok.type;
  }
  // Memory or register pointer
  if (G->tok.type == T_SHARP || G->tok.type == T_DOLLAR) {
    Token_Info tok = G->tok;
    tok.type = (G->tok.type == T_DOLLAR)
      ? T_REGPTR : T_MEMPTR;
    const char *name = (G->tok.type == T_DOLLAR)
      ? "a register index" : "a memory index";
    read_token_solid();
    expect_token(T_INTLIT, name);
    if (G->tok.num < 0) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "ERROR: invalid index\n");
      exit(EXIT_FAILURE);
    }
    if (tok.type == T_REGPTR && G->tok.num == 0) {
      tok.type = T_INTLIT;
      tok.num = 0;
      G->tok = tok;
      return G->tok.type;
    }
    tok.num = G->tok.num;
    G->tok = tok;
    return G->tok.type;
  }
  // Memory or register pointer from identifier
  if (G->tok.type == T_ID) {
    const char *string = get_str(G->tok.si);
    int string_length = strlen(string);
    int first = string[0];
    if (first != 'R' && first != 'M') {
      return G->tok.type;
    }
    bool invalid = false;
    G->tok.num = parse_int(string + 1, string_length - 1, &invalid);
    if (!invalid) {
      G->tok.type = (first == 'R') ? T_REGPTR : T_MEMPTR;
      if (G->tok.type == T_REGPTR && G->tok.num == 0) {
        G->tok.type = T_INTLIT;
        G->tok.num = 0;
      }
    }
    return G->tok.type;
  }
  return G->tok.type;
}

////////////////////////////////////////////////////////////////
int64_t get_compiler_variable(int tok_type)
{
  if (tok_type == T_BITS) {
    return G->h_bits;
  }
  if (tok_type == T_MINHEAP) {
    return G->h_minheap;
  }
  if (tok_type == T_MINREG) {
    return G->h_minreg;
  }
  if (tok_type == T_MINSTACK) {
    return G->h_minstack;
  }
  if (tok_type == T_HEAP) {
    return G->h_minheap;
  }
  uint64_t msb = 1 << (G->h_bits - 1);
  uint64_t smsb = 1 << (G->h_bits - 2);
  uint64_t umax = 0xFFFFFFFFFFFFFFFF >> (64 - G->h_bits);
  uint64_t smax = 0x7FFFFFFFFFFFFFFF >> (64 - G->h_bits);
  uint64_t lowh = umax >> (G->h_bits / 2);
  uint64_t upph = lowh << (G->h_bits / 2);
  if (tok_type == T_MSB) {
    return msb;
  }
  if (tok_type == T_SMSB) {
    return smsb;
  }
  if (tok_type == T_MAX) {
    return umax;
  }
  if (tok_type == T_SMAX) {
    return smax;
  }
  if (tok_type == T_UHALF) {
    return upph;
  }
  if (tok_type == T_LHALF) {
    return lowh;
  }
  fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
  fprintf(stderr, "bug: invalid compiler variable name\n");
  exit(EXIT_FAILURE);
  return 0;
}

////////////////////////////////////////////////////////////////
bool is_instruction(int tok_type)
{
  return tok_type > T_IBEG && tok_type < T_IEND;
}

////////////////////////////////////////////////////////////////
bool is_cvariable(int tok_type)
{
  return G->tok.type > T_VBEG && G->tok.type < T_VEND;
}

////////////////////////////////////////////////////////////////
int read_token_solid(void)
{
  G->tok.type = read_token_maybe();
  if (G->tok.type == T_INVALID) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
    fprintf(stderr, "error: invalid token\n");
    exit(EXIT_FAILURE);
  }
  return G->tok.type;
}

////////////////////////////////////////////////////////////////
int read_token_maybe(void)
{
  char buf[0x10000]; // this is enough for everyone
  char buf2[sizeof(buf)];
  int bufi = 0;
  static struct { int chr; int type; } onechrs[] = {
    { .chr = '\n', .type = T_NEWLINE },
    { .chr = '.', .type = T_DOT },
    { .chr = '[', .type = T_BRACK_BEG },
    { .chr = ']', .type = T_BRACK_END },
    { .chr = '%', .type = T_PERCENT },
    { .chr = '#', .type = T_SHARP },
    { .chr = '@', .type = T_AT },
    { .chr = '$', .type = T_DOLLAR },
    { .chr = '~', .type = T_TILDE },
    { .chr = '\\', .type = T_BACKSLASH },
  };
  static struct { const char *str; int type; } keywords[] = {
    // Pseudo instructions and keywords
    { .str = "DEFINE", .type = T_DEFINE },
    { .str = "DW", .type = T_DW },
    { .str = "PC", .type = T_PC },
    { .str = "BITS", .type = T_BITS },
    { .str = "MINREG", .type = T_MINREG },
    { .str = "MINHEAP", .type = T_MINHEAP },
    { .str = "MINSTACK", .type = T_MINSTACK },
    { .str = "RUN", .type = T_RUN },
    { .str = "RAM", .type = T_RAM },
    { .str = "ROM", .type = T_ROM },
    { .str = "HEAP", .type = T_HEAP },
    { .str = "MSB", .type = T_MSB },
    { .str = "SMSB", .type = T_SMSB },
    { .str = "MAX", .type = T_MAX },
    { .str = "SMAX", .type = T_SMAX },
    { .str = "UHALF", .type = T_UHALF },
    { .str = "LHALF", .type = T_LHALF },
    // Instructions A-Z
    { .str = "ADD", .type = T_ADD },
    { .str = "BGE", .type = T_BGE },
    { .str = "BLE", .type = T_BLE },
    { .str = "BNE", .type = T_BNE },
    { .str = "BRG", .type = T_BRG },
    { .str = "BRL", .type = T_BRL },
    { .str = "BRE", .type = T_BRE },
    { .str = "HLT", .type = T_HLT },
    { .str = "IN",  .type = T_IN },
    { .str = "IMM", .type = T_IMM },
    { .str = "JMP", .type = T_JMP },
    { .str = "LOD", .type = T_LOD },
    { .str = "MOV", .type = T_MOV },
    { .str = "NOP", .type = T_NOP },
    { .str = "NOR", .type = T_NOR },
    { .str = "OUT", .type = T_OUT },
    { .str = "RSH", .type = T_RSH },
    { .str = "STR", .type = T_STR },
    { .str = "SUB", .type = T_SUB },
  };
  int keywords_num = sizeof(keywords) / sizeof(keywords[0]);
  int onechrs_num = sizeof(onechrs) / sizeof(onechrs[0]);
  memset(&G->tok, 0, sizeof(G->tok));
  memset(buf, 0, sizeof(buf));
  memset(buf2, 0, sizeof(buf2));
  G->tok.row = G->chr_row;
  G->tok.col = G->chr_col;
  // Skip shebang
  if (G->chr_row == 1 && G->chr_col == 1 && G->chr == '#') {
    while (G->chr != EOF && G->chr != '\n') {
      read_char();
    }
  }
  // Skip spaces and comments
  for (;;) {
    if (G->chr == ' ') {
      read_char();
      continue;
    }
    if (G->chr != '/') {
      break;
    }
    read_char();
    // C++ comment
    if (G->chr == '/') {
      while (G->chr != EOF && G->chr != '\n') {
        read_char();
      }
      break; // because of '\n'
    }
    // C comment
    if (G->chr == '*') {
      int prev = 0;
      read_char();
      while (G->chr != EOF && !(prev == '*' && G->chr == '/')) {
        prev = G->chr;
        read_char();
      }
      read_char();
      continue;
    }
    return T_INVALID;
  }
  G->tok.row = G->chr_row;
  G->tok.col = G->chr_col;
  if (G->chr == EOF) {
    return T_EOF;
  }
  // One character tokens
  for (int i = 0; i < onechrs_num; i++) {
    if (G->chr == onechrs[i].chr) {
      read_char();
      return onechrs[i].type;
    }
  }
  // <= == >=
  if (G->chr == '<' || G->chr == '=' || G->chr == '>') {
    int type = T_EQUAL;
    if (G->chr == '<') {
      type = T_LESS;
    }
    if (G->chr == '>') {
      type = T_MORE;
    }
    read_char();
    if (G->chr != '=') {
      return T_INVALID;
    }
    read_char();
    return type;
  }
  // Identifier
  if (G->chr == '_' || isalpha(G->chr)) {
    for (bufi = 0; bufi < sizeof(buf) && G->chr != EOF && (G->chr == '_' || isalnum(G->chr)); bufi++) {
      buf[bufi] = G->chr;
      read_char();
    }
    if (bufi >= sizeof(buf) - 1) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: the identifier is too long\n");
      exit(EXIT_FAILURE);
    }
    int buf_length = bufi;
    for (int i = 0; i < buf_length; i++) {
      buf2[i] = toupper(buf[i]);
    }
    for (int i = 0; i < keywords_num; i++) {
      if (strcmp(buf2, keywords[i].str) == 0) {
        return keywords[i].type;
      }
    }
    G->tok.si = get_si(buf);
    return T_ID;
  }
  // Integer literal
  if (G->chr == '+' || G->chr == '-' || isdigit(G->chr)) {
    for (bufi = 0; bufi < sizeof(buf) && G->chr != EOF && (isalnum(G->chr) || G->chr == '+' || G->chr == '-'); bufi++) {
      buf[bufi] = G->chr;
      read_char();
    }
    if (bufi >= sizeof(buf) - 1) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: the number is too long\n");
      exit(EXIT_FAILURE);
    }
    bool invalid = false;
    G->tok.num = parse_int(buf, bufi, &invalid);
    if (invalid) {
      return T_INVALID;
    }
    return T_INTLIT;
  }
  // String or character literal
  if (G->chr == '\"' || G->chr == '\'') {
    int type = (G->chr == '\"') ? T_STRLIT : T_INTLIT;
    int quote = G->chr;
    read_char();
    for (bufi = 0; bufi < sizeof(buf) && G->chr != EOF && G->chr != quote; bufi++) {
      if (!G->f_noescseq && G->chr == '\\') {
        read_char();
        int escaped = escape_sequence(G->chr);
        if (escaped != -1) {
          buf[bufi] = escaped;
          read_char();
          continue;
        }
        else {
          buf[bufi] = '\\';
          continue; // no escape
        }
      }
      buf[bufi] = G->chr;
      read_char();
    }
    if (bufi >= sizeof(buf) - 1) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: the string is too long\n");
      exit(EXIT_FAILURE);
    }
    int buf_length = bufi;
    if (G->chr != quote) {
      fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
      fprintf(stderr, "error: missing ending quote\n");
      exit(EXIT_FAILURE);
    }
    read_char();
    if (type == T_INTLIT) {
      if (buf_length != 1) {
        fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->tok.row, G->tok.col);
        fprintf(stderr, "error: invalid character literal\n");
        exit(EXIT_FAILURE);
      }
      G->tok.num = buf[0];
      return type;
    }
    G->tok.si = get_si(buf);
    return type;
  }
  // Any other token be like:
  return T_INVALID;
}

////////////////////////////////////////////////////////////////
int read_char(void)
{
  if (G->chr == '\n') {
    G->chr_row += 1;
    G->chr_col = 1;
  }
  else {
    G->chr_col += 1;
  }
  G->chr = fgetc(G->source_file);
  bool valid = false
  || (0x20 <= G->chr && G->chr < 0x7F)
  || G->chr == '\n'
  || G->chr == '\t'
  ;
  if (!valid && G->chr != EOF) {
    fprintf(stderr, "%s:%i:%i: ", G->source_filename, G->chr_row, G->chr_col);
    fprintf(stderr, "error: invalid character\n");
    exit(EXIT_FAILURE);
  }
  return G->chr;
}

////////////////////////////////////////////////////////////////
int64_t parse_int(const char *string, int string_length, bool *invalid_out)
{
  if (string_length == 0) {
    *invalid_out = true;
    return 0;
  }
  int64_t signed_one = 1;
  *invalid_out = false;
  if (string[0] == '+' || string[0] == '-') {
    signed_one = (string[0] == '+') ? 1 : -1;
    if (string_length == 1) {
      *invalid_out = true;
      return 0;
    }
    string_length -= 1;
    string += 1;
  }
  int base = 10;
  int i = 0;
  if (string[0] == '0' && string_length > 1) {
    base = 8;
    i = 1;
    int alpha = tolower(string[1]);
    if (alpha == 'x') {
      base = 16;
      i = 2;
    }
    else if (alpha == 'o') {
      base = 8;
      i = 2;
    }
    else if (alpha == 'b') {
      base = 2;
      i = 2;
    }
  }
  int64_t num = 0;
  for (; i < string_length; i++) {
    int digit = char_to_digit(string[i]);
    if (digit < 0 || digit >= base) {
      *invalid_out = true;
      return 0;
    }
    num *= base;
    num += digit;
  }
  return signed_one * num;
}

////////////////////////////////////////////////////////////////
int escape_sequence(int chr)
{
  switch (chr) {
    case 'a': return '\a';
    case 'b': return '\b';
    case 'e': return 0x7F;
    case 'f': return '\f';
    case 'n': return '\n';
    case 'r': return '\r';
    case 't': return '\t';
    case 'v': return '\v';
    case '\\': return '\\';
    case '\'': return '\'';
    case '\"': return '\"';
    case '\0': return '\0';
    default: break;
  }
  return -1;
}

////////////////////////////////////////////////////////////////
int char_to_digit(int digit_char)
{
  digit_char = toupper(digit_char);
  switch (digit_char) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    case 'A': return 10;
    case 'B': return 11;
    case 'C': return 12;
    case 'D': return 13;
    case 'E': return 14;
    case 'F': return 15;
    default: break;
  }
  return -1;
}

/*
MIT License

Copyright (c) 2024 Artem Pirunov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

